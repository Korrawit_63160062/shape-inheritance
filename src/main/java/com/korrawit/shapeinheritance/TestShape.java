/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeinheritance;

/**
 *
 * @author DELL
 */
public class TestShape {

    public static void main(String[] args) {

        Circle circle1 = new Circle(3);

        Circle circle2 = new Circle(4);

        Triangle triangle1 = new Triangle(4, 3);

        Rectangle rectangle1 = new Rectangle(4, 3);

        Square square1 = new Square(2);

        Shape[] shapes = {circle1, circle2, triangle1, rectangle1, square1};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calArea());
        }
    }
}
