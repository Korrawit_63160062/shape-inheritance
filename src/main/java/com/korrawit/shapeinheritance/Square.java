/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeinheritance;

/**
 *
 * @author DELL
 */
public class Square extends Rectangle {


    public Square(double side) {
        super(0, side);
        this.side = side;
    }

    public double calArea() {
        super.calArea();
        System.out.print("Square width: " + side + ", area: ");
        return side * side;
    }
}
