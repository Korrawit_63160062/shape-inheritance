/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeinheritance;

/**
 *
 * @author DELL
 */
public class Circle extends Shape {

    private double pi = 22.0 / 7;

    public Circle(double r) {
        super(r, 0, 0, 0, 0);
        this.r = r;
    }

    public double calArea() {
        super.calArea();
        System.out.print("Circle r: " + r + ", area: ");
        return pi * r * r;
    }

}
