/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeinheritance;

/**
 *
 * @author DELL
 */
public class Triangle extends Shape {



    public Triangle(double base, double height) {
        super(0, base, height, 0, 0);
        this.base = base;
        this.height = height;
    }

    public double calArea() {
        super.calArea();
        System.out.print("Triangle base: " + base + ", height: " + height + ", area: ");
        return 0.5 * base * height;
    }

}
