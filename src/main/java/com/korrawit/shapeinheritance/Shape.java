/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeinheritance;

/**
 *
 * @author DELL
 */
public class Shape {

    protected double r;
    protected double base;
    protected double height;
    protected double width;
    protected double side;

    public Shape(double r, double base, double height, double width, double side) {
        this.r = r;
        this.base = base;
        this.height = height;
        this.width = width;
        this.side = side;
    }

    public double calArea() {
        System.out.println("No data in Parent's calArea");
        return 0;
    }

}
