/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeinheritance;

/**
 *
 * @author DELL
 */
public class Rectangle extends Shape {



    public Rectangle(double width, double height) {
        super(0, 0, height, width, 0);
        this.height = height;
        this.width = width;

    }

    public double calArea() {
        super.calArea();
        System.out.print("Rectangle width: " + width + ", height: " + height + ", area: ");
        return width * height;
    }

}
